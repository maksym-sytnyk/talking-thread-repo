import { connect } from "react-redux";
import { push } from "connected-react-router";
import {
  leaveSession,
  openFullScreen,
  closeFullScreen,
  shareScreen,
  unshareScreen,
  publishVideo,
  publishAudio,
} from "../actions";

import ToolBar from "../components/ToolBar";

const mapDispatchToProps = dispatch => ({
  push,
  openFullScreen: element => dispatch(openFullScreen(element)),
  closeFullScreen: () => dispatch(closeFullScreen()),
  leaveSession: () => dispatch(leaveSession()),
  shareScreen: () => dispatch(shareScreen()),
  unshareScreen: () => dispatch(unshareScreen()),
  publishVideo: () => dispatch(publishVideo()),
  publishAudio: () => dispatch(publishAudio()),
});

export default connect(
  null,
  mapDispatchToProps,
)(ToolBar);
