import { connect } from "react-redux";
import CurrentUsers from "../components/CurrentUsers";

const mapStateToProps = state => ({
  users: state.call.videoReducer.participants,
});

export default connect(mapStateToProps)(CurrentUsers);
