import React from "react";
import Header from "./components/Header";
import Main from "./components/Main";
import Footer from "./components/Footer";

import "./scss/styles.scss";

import Init from "../init";

const LandingPage = () => (
  <Init>
    <div className="App wrapper landing">
      <Header />
      <Main />
      <Footer />
    </div>
  </Init>
);
export default LandingPage;
