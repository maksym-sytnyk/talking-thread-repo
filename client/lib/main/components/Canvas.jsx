import React, { Component } from "react";
import Demo, { DISTANCE_BETWEEN_VERTICES } from "../canvasStyle";

class Canvas extends Component {
  componentDidMount() {
    const demo = new Demo({
      DISTANCE_BETWEEN_VERTICES: 300,
      id: "scene",
      connectionFn: (a, b) => {
        const distance = Math.sqrt(
          // eslint-disable-next-line no-restricted-properties
          Math.pow(b.x - a.x, 2) + Math.pow(b.y - a.y, 2),
        );
        return distance < DISTANCE_BETWEEN_VERTICES;
      },
    });

    demo.mainLoop();
  }

  render() {
    return (
      <div className="wrapper-canvas">
        <canvas id="scene" />
      </div>
    );
  }
}

export default Canvas;
