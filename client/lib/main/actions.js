import { SET_SESSION_NAME } from "./actionTypes";

// eslint-disable-next-line import/prefer-default-export
export const setSessionName = sessionName => dispatch => {
  dispatch({ type: SET_SESSION_NAME, payload: { sessionName } });
};
