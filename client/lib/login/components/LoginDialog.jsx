import React from "react";
import PropTypes from "prop-types";
import ModalDialog, { ModalTransition } from "@atlaskit/modal-dialog";
import SignInForm from "./SignInForm";
import SignUpForm from "./SignUpForm";

const LoginDialog = props => {
  const { isModalOpened, isSignInFormOpened } = props.formState;
  const innerForm = isSignInFormOpened ? "forSignInForm" : "forSignUpForm";

  const actions = {
    forSignInForm: [
      { text: "Log in", type: "submit", form: "signInFormId" },
      { text: "Close", onClick: props.onCloseSignInModal },
    ],
    forSignUpForm: [
      { text: "Creare account", type: "submit", form: "signUpFormId" },
      { text: "Close", onClick: props.onOpenSignInForm },
    ],
  };
  const forms = {
    forSignInForm: () => (
      <SignInForm
        openSignUpForm={() => props.onOpenSignUpForm()}
        onLogin={async (login, password) => {
          if (await props.onLogin(login, password)) {
            props.onCloseSignInModal();
          }
        }}
      />
    ),
    forSignUpForm: () => <SignUpForm createAccount={props.createAccount} />,
  };

  return (
    <ModalTransition>
      {isModalOpened && (
        <ModalDialog
          actions={actions[innerForm]}
          onClose={props.onCloseSignInModal}
          shouldCloseOnOverlayClick={false}
          shouldCloseOnEscapePress={false}
        >
          {forms[innerForm]()}
        </ModalDialog>
      )}
    </ModalTransition>
  );
};

export default LoginDialog;

LoginDialog.propTypes = {
  formState: PropTypes.objectOf(PropTypes.any).isRequired,
  onCloseSignInModal: PropTypes.func.isRequired,
  onOpenSignInForm: PropTypes.func.isRequired,
  // eslint-disable-next-line react/no-unused-prop-types
  onOpenSignUpForm: PropTypes.func.isRequired,
  // eslint-disable-next-line react/no-unused-prop-types
  onLogin: PropTypes.func.isRequired,
  // eslint-disable-next-line react/no-unused-prop-types
  createAccount: PropTypes.func.isRequired,
};
