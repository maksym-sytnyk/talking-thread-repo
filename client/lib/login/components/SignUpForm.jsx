import React, { Fragment } from "react";
import PropTypes from "prop-types";
import TextField from "@atlaskit/textfield";
import Form, {
  Field,
  HelperMessage,
  ErrorMessage,
  ValidMessage,
} from "@atlaskit/form";
import PageHeader from "@atlaskit/page-header";

const SignUpForm = props => {
  let firstPass;
  return (
    <div
      style={{
        display: "flex",
        width: "400px",
        margin: "0 auto",
        flexDirection: "column",
      }}
    >
      <PageHeader>Sign up for an account</PageHeader>
      <Form
        onSubmit={data => {
          props.createAccount(data);
          return false;
        }}
      >
        {({ formProps }) => (
          <form {...formProps} id="signUpFormId">
            <Field
              className="signUp-form signUp-form__userName-input"
              id="userName"
              name="userName"
              isRequired
              defaultValue=""
              label="Name"
              validate={value => value.length < 2 && "Use 2 or more characters"}
            >
              {({ fieldProps, error, meta }) => (
                <Fragment>
                  <TextField type="text" autoComplete="off" {...fieldProps} />
                  {!error && !meta.valid && (
                    <HelperMessage>
                      Use 2 or more characters (letters, numbers or periods)
                    </HelperMessage>
                  )}
                  {error && (
                    <ErrorMessage>
                      Login needs to be no less than 2 characters
                    </ErrorMessage>
                  )}
                  {meta.valid && <ValidMessage>That&#39;s Ok</ValidMessage>}
                </Fragment>
              )}
            </Field>
            <Field
              className="signUp-form signUp-form__userLogin-input"
              id="userLogin"
              name="userLogin"
              isRequired
              defaultValue=""
              label="Login"
              validate={value => value.length < 5 && "Use 5 or more characters"}
            >
              {({ fieldProps, error, meta }) => (
                <Fragment>
                  <TextField type="text" autoComplete="off" {...fieldProps} />
                  {!error && !meta.valid && (
                    <HelperMessage>
                      Use 5 or more characters (letters, numbers or periods)
                    </HelperMessage>
                  )}
                  {error && (
                    <ErrorMessage>
                      Login needs to be no less than 5 characters
                    </ErrorMessage>
                  )}

                  {meta.valid && <ValidMessage>That&#39;s Ok</ValidMessage>}
                </Fragment>
              )}
            </Field>
            <Field
              name="userPassword"
              className="signUp-form signUp-form__userPassword-input"
              id="userPassword"
              label="Password"
              defaultValue=""
              isRequired
              validate={value => value.length < 4 && "Use 4 or more characters"}
            >
              {({ fieldProps, error, meta }) => {
                firstPass = fieldProps.value;

                return (
                  <Fragment>
                    <TextField type="password" {...fieldProps} />
                    {!error && !meta.valid && (
                      <HelperMessage>
                        Use 4 or more characters with a mix of letters, numbers
                        or symbols
                      </HelperMessage>
                    )}
                    {error && (
                      <ErrorMessage>
                        Password needs to be no less than 4 characters
                      </ErrorMessage>
                    )}
                    {meta.valid && (
                      <ValidMessage>Awesome password!</ValidMessage>
                    )}
                  </Fragment>
                );
              }}
            </Field>
            <Field
              name="userPasswordConfirm"
              className="signUp-form signUp-form__userPasswordCheck-input"
              id="userPasswordConfirm"
              label="Confirm password"
              defaultValue=""
              isRequired
              validate={value =>
                value !== firstPass || value === ""
                  ? "Password and password confirm should be identical"
                  : undefined
              }
            >
              {({ fieldProps, error, meta }) => (
                <Fragment>
                  <TextField type="password" {...fieldProps} />
                  {!error && !meta.valid && (
                    <HelperMessage>
                      Please enter the password once again
                    </HelperMessage>
                  )}
                  {error && (
                    <ErrorMessage>
                      Password needs to be the same as the previous one
                    </ErrorMessage>
                  )}
                  {meta.valid && (
                    <ValidMessage>Password is confirmed</ValidMessage>
                  )}
                </Fragment>
              )}
            </Field>
          </form>
        )}
      </Form>
    </div>
  );
};

SignUpForm.propTypes = {
  createAccount: PropTypes.func.isRequired,
};

export default SignUpForm;
