import React, { Fragment } from "react";
import PropTypes from "prop-types";
import Button from "@atlaskit/button";
import TextField from "@atlaskit/textfield";
import Form, { Field, HelperMessage } from "@atlaskit/form";
import PageHeader from "@atlaskit/page-header";

const SignInForm = props => {
  const { openSignUpForm, onLogin } = props;

  return (
    <div
      style={{
        display: "flex",
        width: "400px",
        margin: "0 auto",
        flexDirection: "column",
      }}
    >
      <PageHeader>Log in to your account</PageHeader>
      <Form
        className="signIn-form jumbotron"
        onSubmit={formData => {
          onLogin(formData.login, formData.password);
        }}
      >
        {({ formProps }) => (
          <form {...formProps} id="signInFormId">
            <Field
              className="signIn-form signIn-form__login-input"
              id="login"
              name="login"
              isRequired
              defaultValue=""
              label="Login"
            >
              {({ fieldProps }) => (
                <Fragment>
                  <TextField type="text" autoComplete="off" {...fieldProps} />
                  <HelperMessage>
                    Use any characters (letters, numbers or periods)
                  </HelperMessage>
                </Fragment>
              )}
            </Field>

            <Field
              name="password"
              className="signIn-form signIn-form__password-input"
              id="password"
              label="Password"
              defaultValue=""
              isRequired
            >
              {({ fieldProps }) => (
                <Fragment>
                  <TextField type="password" {...fieldProps} />
                  <HelperMessage>
                    Use any characters (letters, numbers or periods)
                  </HelperMessage>
                </Fragment>
              )}
            </Field>
            {/* eslint-disable-next-line react/no-unescaped-entities */}
            <span
              style={{
                display: "inline-block",
                width: "240px",
                textAlign: "right",
                paddingRight: "20px",
                paddingLeft: "20px",
                paddingTop: "50px",
              }}
            >
              {/* eslint-disable-next-line react/no-unescaped-entities */}
              Haven't got an account yet?
            </span>
            <Button
              style={{
                display: "inline-block",
                textAlign: "right",
              }}
              appearance="link"
              onClick={openSignUpForm}
            >
              {/* eslint-disable-next-line react/no-unescaped-entities */}
              Create, it's free
            </Button>
            {/* eslint-disable-next-line react/no-unescaped-entities */}
          </form>
        )}
      </Form>
    </div>
  );
};

export default SignInForm;

SignInForm.propTypes = {
  openSignUpForm: PropTypes.func.isRequired,
  onLogin: PropTypes.func.isRequired,
};
