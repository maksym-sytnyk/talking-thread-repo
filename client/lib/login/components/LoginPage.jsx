import React, { Fragment } from "react";
import PropTypes from "prop-types";
import Button, { ButtonGroup } from "@atlaskit/button";
import SignInForm from "./SignInForm";
import SignUpForm from "./SignUpForm";

const LoginPage = props => {
  const { isSignInFormOpened } = props.formState;
  const innerForm = isSignInFormOpened ? "forSignInForm" : "forSignUpForm";

  const buttonGroups = {
    forSignInForm: (
      <ButtonGroup>
        <Button appearance="primary" type="submit" form="signInFormId">
          Log in
        </Button>
      </ButtonGroup>
    ),
    forSignUpForm: (
      <ButtonGroup>
        <Button appearance="primary" type="submit" form="signUpFormId">
          Create account
        </Button>

        <Button
          appearance="default"
          type="button"
          onClick={props.onOpenSignInForm}
        >
          Close
        </Button>
      </ButtonGroup>
    ),
  };

  const forms = {
    forSignInForm: (
      <SignInForm
        openSignUpForm={() => props.onOpenSignUpForm()}
        onLogin={(login, name, password) => {
          props.onLogin(login, name, password);
        }}
      />
    ),
    forSignUpForm: <SignUpForm createAccount={props.createAccount} />,
  };

  return (
    <div style={{ width: "400px", margin: "0 auto" }}>
      <Fragment>
        {forms[innerForm]}
        <div style={{ textAlign: "right", marginTop: "50px" }}>
          {buttonGroups[innerForm]}
        </div>
      </Fragment>
    </div>
  );
};
export default LoginPage;

LoginPage.propTypes = {
  formState: PropTypes.objectOf(PropTypes.any).isRequired,
  onOpenSignInForm: PropTypes.func.isRequired,
  onOpenSignUpForm: PropTypes.func.isRequired,
  onLogin: PropTypes.func.isRequired,
  createAccount: PropTypes.func.isRequired,
};
