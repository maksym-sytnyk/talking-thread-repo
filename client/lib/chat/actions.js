import { MESSAGE_RECEIVED, RECIEVED_MESSAGES_LIST } from "./actionsTypes";
import sockets from "../sessions/sockets";

// eslint-disable-next-line import/prefer-default-export
export const addMessage = msgBody => () => {
  sockets.emit("messageSent", { message: msgBody });
};

export const messageReceived = ({ message, user }) => ({
  type: MESSAGE_RECEIVED,
  message,
  user,
});

export const recievedMessagesList = messages => ({
  type: RECIEVED_MESSAGES_LIST,
  messages,
});

export const resetMessages = () => recievedMessagesList([]);
