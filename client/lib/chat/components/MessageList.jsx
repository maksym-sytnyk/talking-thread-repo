import React, { Fragment } from "react";
import PropTypes from "prop-types";

import Message from "./Message";
import SystemMessage from "./SystemMessage";

const MessageList = props => {
  const messages = props.messages.map((message, index) => {
    if (message.author === "system") {
      // eslint-disable-next-line no-console
      console.log("SYSTEM MESSAGE");
      // eslint-disable-next-line react/no-array-index-key
      return <SystemMessage key={index} msgBody={message.msgBody} />;
    }
    return (
      // eslint-disable-next-line react/no-array-index-key
      <Message key={index} author={message.author} msgBody={message.msgBody} />
    );
  });
  return <Fragment>{messages}</Fragment>;
};

MessageList.propTypes = {
  messages: PropTypes.arrayOf(PropTypes.object),
};

export default MessageList;
