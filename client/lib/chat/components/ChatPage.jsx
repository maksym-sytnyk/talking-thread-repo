import React from "react";
import Inputfield from "../containers/InputField";
import MessageList from "../containers/MessageList";

const ChatPage = () => (
  <div className="chat">
    <div className="messages">
      <MessageList />
    </div>
    <Inputfield />
  </div>
);

export default ChatPage;
