const express = require("express");
const router = express.Router();
const Services = require("../services");
const userValidate = require("../services/validatorServices/UserValidatorService");
const passport = require("../services/Auth/checkAuth");
//RETURN ALL
router.get(
  "/",
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const getUsers = new Services.UserServices.GetUsersService();
    const users = await getUsers.execute();
    if (users) {
      res.json(users);
    } else {
      res.sendStatus(404);
    }
  }
);

//FIND BY ID
router.get(
  "/:id",
  async (req, res, next) => {
    if (Number.isInteger(Number(req.params.id))) {
      next();
    } else {
      res.sendStatus(400);
    }
  },
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const getUser = new Services.UserServices.GetUserService();
    const user = await getUser.execute(req.params.id);
    if (user) {
      res.status(200).json(user);
    } else {
      res.sendStatus(404);
    }
  }
);

//CREATE USER
router.post(
  "/",
  async (req, res, next) => {
    const result = await userValidate.execute(req.body);
    if (result) {
      next();
    } else {
      res.sendStatus(400);
    }
  },
  async (req, res) => {
    const createUser = new Services.UserServices.CreateUserService();
    const user = await createUser.execute(req.body);
    console.log(user);
    if (user) {
      res.json(user);
    } else {
      res.sendStatus(409);
    }
  }
);
//UPDATE USER
router.put(
  "/:id",
  async (req, res, next) => {
    const user = await userValidate.execute(req.body);
    if (user && Number.isInteger(Number(req.params.id))) {
      next();
    } else {
      res.sendStatus(400).end();
    }
  },
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const updateUser = new Services.UserServices.UpdateUserService();
    const user = await updateUser.execute(req.params.id, req.body);
    if (user) {
      res.json(user);
    } else {
      res.sendStatus(409);
    }
  }
);
//DELETE USER
router.delete(
  "/:id",
  async (req, res, next) => {
    if (Number.isInteger(Number(req.params.id))) {
      next();
    } else {
      res.sendStatus(400);
    }
  },
  passport.authenticate("jwt", { session: false }),
  async (req, res) => {
    const deleteUser = new Services.UserServices.DeleteUserService();
    const user = await deleteUser.execute(req.params.id);
    if (user) {
      res.sendStatus(200);
    } else {
      res.sendStatus(404);
    }
  }
);

module.exports = router;
