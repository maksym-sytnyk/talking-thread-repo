const database = require("../../database");
class UpdateUser {
  async execute(id, body) {
    let result = false;
    try {
      const user = await database.User.findOne({
        where: { id },
        attributes: {
          exclude: ["password", "refreshToken", "token"],
        },
      });
      if (user !== null) {
        if (body.password) {
          delete body.password;
        }
        await user.update(body);
        console.log("User updated");
        result = user;
      } else {
        console.log("User does not exist");
      }
    } catch (err) {
      console.log(err);
    }
    return result;
  }
}

module.exports = UpdateUser;
