class UserLogout {
  async execute(user) {
    let result = false;
    user.update({ refreshToken: null });
    console.log("User " + user.login + " logged out");
    result = true;

    return result;
  }
}

module.exports = UserLogout;
