const database = require("../../database");

class CreateMessage {
  async execute(roomName, userId, message) {
    const user = await database.User.findOne({ where: { id: userId } });
    const room = await database.Room.findOne({ where: { name: roomName } });
    let result = false;
    if (user && room) {
      result = await database.Message.create({
        value: message,
        roomId: room.id,
        userId,
      });
    }
    return result;
  }
}

module.exports = CreateMessage;
