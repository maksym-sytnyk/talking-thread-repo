const database = require("../../database");

class UpdateMessage {
  async execute(roomId, userId, message, updatedMessage) {
    let result = false;
    const msg = await database.Message.findOne({
      roomId,
      userId,
      value: message,
    });
    if (msg) {
      try {
        msg.update({ value: updatedMessage });
        console.log("updated");
        result = true;
      } catch (err) {
        console.log(err);
      }
    }
    return result;
  }
}

module.exports = UpdateMessage;
