const database = require("../../database");
const MapSessions = require("../MapSessionsService");

class DeleteRoom {
  async execute(id) {
    let res = false;
    try {
      const room = await database.Room.findById(id);
      if (room) {
        room.destroy();
        MapSessions.del(room.name);
        console.log("Room #" + id + " deleted");
        res = true;
      } else {
        res = false;
        console.log("Room does not exist");
      }
    } catch (err) {
      console.log(err);
    }
    return res;
  }
}

module.exports = DeleteRoom;
