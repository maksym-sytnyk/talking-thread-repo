const database = require("../../database");

class GetUsersFromRoom {
  async execute(name) {
    let result = false;
    try {
      const room = await database.Room.findOne({
        where: { name },
        include: [
          {
            model: database.User,
            attributes: { exclude: ["password", "refreshToken", "token"] },
          },
        ],
      });
      if (room.users) {
        result = room.users;
      }
    } catch (err) {
      console.log(err);
    }
    return result;
  }
}

module.exports = GetUsersFromRoom;
