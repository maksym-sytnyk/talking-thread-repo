const bcrypt = require("bcryptjs");
const cfg = require("../hash/config");
const faker = require("faker");

const logins = ["user1", "user2", "user3", "user4", "user5"];

module.exports = {
  up: queryInterface =>
    queryInterface.bulkInsert(
      "users",
      logins.map(login => ({
        login,
        name: faker.name.findName(),
        // eslint-disable-next-line no-sync
        password: bcrypt.hashSync("pass", cfg.saltRounds),
        createdAt: new Date(),
        updatedAt: new Date(),
      })),
      {}
    ),

  down: queryInterface => queryInterface.bulkDelete("users", null, {}),
};
