# Talking Thread

Ребята, если кому-то интересно запустить пример с openvidu https://bitbucket.org/softservewebui1912/talking-thread-repo/src/master/
то вам для этого понадобится npm и Docker.
Изначально информацию по запуску примера можно посмотреть тут https://openvidu.io/docs/tutorials/openvidu-js-node/
Сразу оговорюсь, что если у вас Windows, то команды будут немного другие, их можно найти здесь https://openvidu.io/docs/troubleshooting/#3-i-am-using-windows-to-run-the-tutorials-develop-my-app-anything-i-should-know

Последовательность примерно следующая:

1.	Клонирование проекта с https://bitbucket.org/softservewebui1912/talking-thread-repo/src/master/
1.	Я думаю, что node, npm установлены у всех поэтому этот шаг мы пропускаем
1.	Открываем терминал *в папке проекта* и вызываем `npm install` в папках *server* и *client*
1.  Собираем фронтенд при помощи команд `npm run build` или `npm run watch`
1.	Далее переходим к Docker. Открываем терминал Docker-а (Если у вас Windows 10, то вам вероятно нужно открыть PowerShell). Если вы поднимаете проект с нуля, вам нужно вызывать `npm run docker:build`, чтобы собрать образы контейнеров, которые нужны для приложения.
1.  Если вы уже запускали `npm run docker:build` вы можете просто запустить контейнеры при помощи команды `npm run docker:start`.
    * После того, как вы соберете проект, база данных, в которой хранятся пользователи и комнаты со звонками будет пустой, поэтому вы можете сгенерировать фейковые данные и записать их в базу при помощи команды `npm run docker:db:seed`. После запуска этой команды в базе данных будут пользователи с логинами от *user1* до *user5* с одинаковыми паролями *pass* и случайными именами.
1.	Далее в браузере открываем страницу по адресу https://localhost:5000. Обратите внимание, что протокол https, а не http.
1.	Когда вы запускаете впервые, то браузер должен вас предупредить, что это небезопасное подключение. Поэтому нужно внести данный сайт в список исключений (это можно сделать нажав на одну из ссылок на страницу, кажется она будет называться "Дополнительно").

Официальный сайт Docker, где можно найти Docker для вашей операционной системы https://hub.docker.com/search/?type=edition&offering=community

